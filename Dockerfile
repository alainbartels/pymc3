FROM ubuntu

#Set the working directory to /files
WORKDIR /files

#Add files
ADD . /files

#Install any needed packages specified in requirements.txt
RUN apt-get update
RUN apt-get install --assume-yes python3
RUN apt-get install --assume-yes python3-pip libblas-dev

#Add git repos
RUN apt-get install --assume-yes git
RUN git clone https://github.com/ABartels13/HelloJupyterStan.git

#Install requirements
RUN apt-get install --assume-yes build-essential libxml2-dev libcurl4-openssl-dev libssl-dev
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

# Make port 8888 available to the world outside this container
EXPOSE 8888

#Run a command
CMD ["jupyter", "lab","--ip=0.0.0.0","--port=8888","--allow-root"]